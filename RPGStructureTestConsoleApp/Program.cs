﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacterStructure;
using RPGCharacterStructure.CharacterType;
using RPGCharacterStructure.CharacterType.MageType;
using RPGCharacterStructure.CharacterType.RogueType;
using RPGCharacterStructure.CharacterType.WarriorType;

namespace RPGStructureTestConsoleApp
{
    class Program
    {
        public static List<Character> characters = new List<Character>();
        static void Main(string[] args)
        {
            characters.Add(new ArmsWarrior("TestWarrior"));
            characters.Add(new FuryWarrior("TestWarrior"));
            characters.Add(new ProtectionWarrior("TestWarrior"));
            characters.Add(new SubtletyRogue("TestRogue"));
            characters.Add(new CombatRogue("TestRogue"));
            characters.Add(new AssasinationRogue("TestRogue"));
            characters.Add(new FrostMage("TestMage"));
            characters.Add(new FireMage("TestMage"));
            characters.Add(new ArcaneMage("TestMage"));


            foreach (var hero in characters){
                hero.ConfigureArmor();
                hero.ConfigureWeapon();
                Console.WriteLine($"\n#############################");
                Console.WriteLine($"Name: {hero.Name}");
                Console.WriteLine($"Type: {hero.Type}");
                Console.WriteLine($"Hp: {hero.Hp}");
                Console.WriteLine($"MaxHp: {hero.MaxHp}");
                Console.WriteLine($"CanMove: {hero.CanMove}");
                Console.WriteLine($"MovingSpeed: {hero.MovingSpeed}");
                Console.WriteLine($"CanMove: {hero.CanMove}");
                Console.WriteLine($"MovingSpeed: {hero.MovingSpeed}");
                Console.WriteLine($"MaxMovingSpeed: {hero.MaxMovingSpeed}");
                Console.WriteLine($"CanAttack: {hero.CanAttack}");
                Console.WriteLine($"WeaponDamagePerSecond: {hero.WeaponDamagePerSecond}");
                Console.WriteLine($"MaxAttackDamage: {hero.MaxAttackDamage}");
                Console.WriteLine($"MinAttackDamage: {hero.MinAttackDamage}");
                Console.WriteLine($"AttackChance: {hero.AttackChance}");
                Console.WriteLine($"MinAttackDamage: {hero.MinAttackDamage}");
                Console.WriteLine($"Armor: {hero.Armor}");
                Console.WriteLine($"MagicResist: {hero.MagicResist}");

                switch (hero.enumType)
                {
                    case CharacterTypeEnumToHelpCasting.Rogue:
                        Console.WriteLine($"Energy: {((Rogue)hero).Energy}");
                        Console.WriteLine($"MaxEnergy: {((Rogue)hero).MaxEnergy}");
                        Console.WriteLine($"Energy: {((Rogue)hero).EnergyUsePerAttack}");
                        break;
                    case CharacterTypeEnumToHelpCasting.Warrior:
                        Console.WriteLine($"Rage: {((Warrior)hero).Rage}");
                        Console.WriteLine($"MaxRage: {((Warrior)hero).MaxRage}");
                        Console.WriteLine($"RageUsePerAttack: {((Warrior)hero).RageUsePerAttack}");
                        break;
                    case CharacterTypeEnumToHelpCasting.Mage:
                        Console.WriteLine($"Mana: {((Mage)hero).Mana}");
                        Console.WriteLine($"MaxMana: {((Mage)hero).MaxMana}");
                        Console.WriteLine($"ManaUsePerAttack: {((Mage)hero).ManaUsePerAttack}");
                        break;
                    default:
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}

﻿using RPGCharacterStructure.CharacterType.RogueType;
using RPGCharacterStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType
{
    public abstract class Rogue : Character, IEnergy
    {
        public int Energy{ get; set; }
        public int MaxEnergy { get; set; }
        public int EnergyUsePerAttack { get; set; }
        /// <summary>
        /// Top level character type
        /// </summary>
        /// <param name="characterName"></param>
        /// <param name="hp"></param>
        /// <param name="movingSpeed"></param>
        /// <param name="maxAttackDamage"></param>
        /// <param name="minAttackDamage"></param>
        /// <param name="attackChance"></param>
        /// <param name="armor"></param>
        /// <param name="magicResist"></param>
        public Rogue(string characterName,
            int hp,
            int movingSpeed,
            int maxAttackDamage,
            int minAttackDamage,
            int attackChance,
            int armor,
            int magicResist)
        {
            Name = characterName;
            MaxHp = hp;
            Hp = hp;
            MovingSpeed = movingSpeed;
            MaxMovingSpeed = movingSpeed;
            MaxAttackDamage = maxAttackDamage;
            MinAttackDamage = minAttackDamage;
            AttackChance = attackChance;
            Armor = armor;
            MagicResist = magicResist;
            CanAttack = true;
            CanMove = true;
            enumType = CharacterTypeEnumToHelpCasting.Rogue;
        }

        public override void ConfigureArmor()
        {
            //Clear list to not have duplicates
            ArmorSet.Clear();
            foreach (var armorName in Enum.GetNames(typeof(ArmorNameEnumRogue)))
            {
                ArmorSet.Add(new Armor(
                    armorName: armorName,
                    armorValue: randomGenerator.Next(5, 10),
                    magicResist: randomGenerator.Next(0, 10),
                    speedAttribute: randomGenerator.Next(-10, 10),
                    extraDamage: randomGenerator.Next(0, 5),
                    extraEnergy: randomGenerator.Next(5, 10),
                    hitChance: randomGenerator.Next(10, 20)));
            }
            //Update character attributes
            UpdateValuesFromArmor();
        }
        public override void UpdateValuesFromArmor()
        {
            Armor = GetAllArmorValues();
            MagicResist += GetAllMagicResist();
            MovingSpeed += GetAllSpeedAttributes();
            MaxAttackDamage += GetAllExtraDamage();
            MinAttackDamage += GetAllExtraDamage();
            MaxEnergy += GetAllExtraEnergy();
            AttackChance += GetAllHitChance();
        }
        

        public int IncreaseEnergy(int energyGained)
        {
            Energy += energyGained;
            if (Energy >= MaxEnergy) Energy = MaxEnergy;
            return Energy;
        }

        public int ReduceEnergy(int eneryUsed)
        {
            Energy -= eneryUsed;
            if (eneryUsed <= 0) Energy = 0;
            return Energy;
        }

        public void SetDefaultEnergy(int maxEnergy)
        {
            MaxEnergy = maxEnergy;
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.MageType
{
    /// <summary>
    /// Enum for available weapons maegs can have
    /// will probably be randomly selected
    /// </summary>
    public enum WeaponNameEnumMage
    {
        TwoHandStaff,
        OneHandStaff,
        Dagger,
        Knife,
        Chime,
        Scepter
    }
}

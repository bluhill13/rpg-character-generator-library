﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.MageType
{
    public class ArcaneMage : Mage
    {
        /// <summary>
        /// Constructor where user of dll can choose himself
        /// what default values should be or just provide a name
        /// and use default values already provided
        /// </summary>
        /// <param name="characterName">required</param>
        /// <param name="hp">not required</param>
        /// <param name="movingSpeed">not required</param>
        /// <param name="maxAttackDamage">not required</param>
        /// <param name="minAttackDamage">not required</param>
        /// <param name="attackChance">not required</param>
        /// <param name="armor">not required</param>
        /// <param name="magicResist">not required</param>
        /// <param name="maxMana">not required</param>
        /// <param name="manaUsePerAttack">not required</param>
        public ArcaneMage(
            string characterName,
            int hp = 1000,
            int movingSpeed = 100,
            int maxAttackDamage = 50,
            int minAttackDamage = 20,
            int attackChance = 10,
            int armor = 100,
            int magicResist = 500,
            int maxMana = 1000,
            int manaUsePerAttack = 50) : base(
                                         characterName,
                                         hp,
                                         movingSpeed,
                                         maxAttackDamage,
                                         minAttackDamage,
                                         attackChance,
                                         armor,
                                         magicResist)
        {
            Type = this.GetType().Name;
            MaxMana = maxMana;
            Mana = maxMana;
            ManaUsePerAttack = manaUsePerAttack;

        }
        public override void ConfigureWeapon()
        {

            //Clear list to not have duplicates
            WeaponSet.Clear();
            //Mage can have two weapons, can change later :) 
            for (int i = 0; i < MAX_WEAPONS; i++)
            {
                double temp = randomGenerator.NextDouble();
                double speed = temp * (double)randomGenerator.Next(2, 4);
                WeaponSet.Add(new Weapon(
                    weaponName: Enum.GetName(typeof(WeaponNameEnumMage), randomGenerator.Next(0, 5)),
                    weaponDamage: randomGenerator.Next(100, 200),
                    magicResist: randomGenerator.Next(0, 10),
                    weaponSpeed: speed,
                    extraMana: randomGenerator.Next(0, 10),
                    hitChance: randomGenerator.Next(10, 20),
                    mageWeapon: true));
            }

            //Update character attributes
            UpdateValuesFromWeapon();
        }
        public void UpdateValuesFromWeapon()
        {
            WeaponDamagePerSecond = GetTotalWeaponDamagePerSecond();
            MaxAttackDamage += GetAllWeaponDamage();
            MinAttackDamage += GetAllWeaponDamage();
            MagicResist += GetAllMagicResistWeapon();
            MaxMana += GetAllExtraManaWeapon();
            AttackChance += GetAllHitChanceWeapon();
        }

        public override bool IsWeaponForMage()
        {
            foreach (Weapon piece in WeaponSet)
            {
                if (piece.MageWeapon) return true;
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.MageType
{
    /// <summary>
    /// Enum for generating armor for mages
    /// </summary>
    public enum ArmorNameEnumMage
    {
        Hood,
        Cloak,
        Bathrobe,
        Trouser,
        Sneakers
    }
}

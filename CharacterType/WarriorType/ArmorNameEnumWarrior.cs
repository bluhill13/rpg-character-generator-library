﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.WarriorType
{
    /// <summary>
    /// Enum for warrior armor
    /// </summary>
    public enum ArmorNameEnumWarrior
    {
        SteelHelmet,
        SteelShoulders,
        SteelChest,
        SteelTrouser,
        SteelShoes
    }
}

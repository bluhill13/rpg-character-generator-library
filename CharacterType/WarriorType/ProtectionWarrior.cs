﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.WarriorType
{
    public class ProtectionWarrior : Warrior
    {
        
        public ProtectionWarrior(
            string characterName,
            int hp = 7000,
            int movingSpeed = 100,
            int maxAttackDamage = 50,
            int minAttackDamage = 20,
            int attackChance = 20,
            int armor = 1500,
            int magicResist = 1500,
            int maxRage = 2000,
            int rageUsePerAttack = 50) : base(
                                         characterName,
                                         hp,
                                         movingSpeed,
                                         maxAttackDamage,
                                         minAttackDamage,
                                         attackChance,
                                         armor,
                                         magicResist)
        {
            Type = this.GetType().Name;
            MaxRage = maxRage;
            Rage = maxRage;
            RageUsePerAttack = rageUsePerAttack;

        }
        public override void ConfigureWeapon()
        {
            //Clear list to not have duplicates
            WeaponSet.Clear();
            //Protection warrior can have one one-hand weapons with a shield 
            //therefore 0 to 2 in weapon name random generator for enum
            for (int i = 0; i < MAX_WEAPONS; i++)
            {
                double temp = randomGenerator.NextDouble();
                if (temp == 0.0) temp = 0.1;
                if (i == 0) WeaponSet.Add(new Weapon(
                   weaponName: Enum.GetName(typeof(WeaponNameEnumWarrior), randomGenerator.Next(3, 5)),
                   weaponDamage: randomGenerator.Next(30, 70),
                   magicResist: randomGenerator.Next(0, 10),
                   weaponSpeed: temp,
                   extraRage: randomGenerator.Next(0, 10),
                   hitChance: randomGenerator.Next(5, 10)));
                else WeaponSet.Add(new Weapon(
                   weaponName: "Shield",
                   weaponDamage: 1,
                   magicResist: randomGenerator.Next(100, 200),
                   weaponSpeed: 1.0,
                   extraRage: randomGenerator.Next(100, 200),
                   hitChance: randomGenerator.Next(100, 100)));

            }

            //Update character attributes
            UpdateValuesFromWeapon();
        }
        public void UpdateValuesFromWeapon()
        {
            WeaponDamagePerSecond = GetTotalWeaponDamagePerSecond();
            MaxAttackDamage += GetAllWeaponDamage();
            MinAttackDamage += GetAllWeaponDamage();
            MagicResist += GetAllMagicResistWeapon();
            MaxRage += GetAllExtraRageWeapon();
            AttackChance += GetAllHitChanceWeapon();
        }

        public override bool IsWeaponForMage()
        {
            foreach (Weapon piece in WeaponSet)
            {
                if (piece.MageWeapon) return true;
            }
            return false;
        }
    }
}

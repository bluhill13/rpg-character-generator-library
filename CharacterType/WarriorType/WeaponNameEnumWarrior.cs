﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.WarriorType
{
    /// <summary>
    /// Enum for weapon choices for warrior
    /// </summary>
    public enum WeaponNameEnumWarrior
    {
        TwoHandSword,
        TwoHandAxe,
        TwoHandMace,
        OneHandSword,
        OneHandAxe,
        OneHandMace
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.WarriorType
{
    public class FuryWarrior : Warrior
    {
        public FuryWarrior(
            string characterName,
            int hp = 5000,
            int movingSpeed = 100,
            int maxAttackDamage = 70,
            int minAttackDamage = 40,
            int attackChance = 20,
            int armor = 1000,
            int magicResist = 1000,
            int maxRage = 1500,
            int rageUsePerAttack = 100) : base(
                                         characterName,
                                         hp,
                                         movingSpeed,
                                         maxAttackDamage,
                                         minAttackDamage,
                                         attackChance,
                                         armor,
                                         magicResist)
        {
            Type = this.GetType().Name;
            MaxRage = maxRage;
            Rage = maxRage;
            RageUsePerAttack = rageUsePerAttack;

        }

        public override void ConfigureWeapon()
        {
            //Clear list to not have duplicates
            WeaponSet.Clear();
            //Fury warrior can have two one-hand weapons with reduced stats 
            //therefore 3 to 5 in weapon name random generator for enum
            for (int i = 0; i < MAX_WEAPONS; i++)
            {
                double temp = randomGenerator.NextDouble();
                if (temp == 0.0) temp = 0.1;
                WeaponSet.Add(new Weapon(
                    weaponName: Enum.GetName(typeof(WeaponNameEnumWarrior), randomGenerator.Next(3, 5)),
                    weaponDamage: randomGenerator.Next(30, 70),
                    magicResist: randomGenerator.Next(0, 10),
                    weaponSpeed: temp,
                    extraRage: randomGenerator.Next(0, 10),
                    hitChance: randomGenerator.Next(5, 10)));
            }
            //Update character attributes
            UpdateValuesFromWeapon();
        }
        public void UpdateValuesFromWeapon()
        {
            WeaponDamagePerSecond = GetTotalWeaponDamagePerSecond();
            MaxAttackDamage += GetAllWeaponDamage();
            MinAttackDamage += GetAllWeaponDamage();
            MagicResist += GetAllMagicResistWeapon();
            MaxRage += GetAllExtraRageWeapon();
            AttackChance += GetAllHitChanceWeapon();
        }

        public override bool IsWeaponForMage()
        {
            foreach (Weapon piece in WeaponSet)
            {
                if (piece.MageWeapon) return true;
            }
            return false;
        }
    }
}

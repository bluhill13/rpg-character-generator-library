﻿using RPGCharacterStructure.CharacterType.MageType;
using RPGCharacterStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType
{
    public abstract class Mage : Character, IMana
    {
        public int Mana { get; set; }
        public int MaxMana { get; set; }
        public int ManaUsePerAttack { get; set; }
        /// <summary>
        /// Top level character type
        /// </summary>
        /// <param name="characterName"></param>
        /// <param name="hp"></param>
        /// <param name="movingSpeed"></param>
        /// <param name="maxAttackDamage"></param>
        /// <param name="minAttackDamage"></param>
        /// <param name="attackChance"></param>
        /// <param name="armor"></param>
        /// <param name="magicResist"></param>
        public Mage(string characterName,
            int hp,
            int movingSpeed,
            int maxAttackDamage,
            int minAttackDamage,
            int attackChance,
            int armor,
            int magicResist)
        {
            Name = characterName;
            MaxHp = hp;
            Hp = hp;
            MovingSpeed = movingSpeed;
            MaxMovingSpeed = movingSpeed;
            MaxAttackDamage = maxAttackDamage;
            MinAttackDamage = minAttackDamage;
            AttackChance = attackChance;
            Armor = armor;
            MagicResist = magicResist;
            CanAttack = true;
            CanMove = true;
            enumType = CharacterTypeEnumToHelpCasting.Mage;
        }
        public override void ConfigureArmor()
        {
            //Clear list to not have duplicates
            ArmorSet.Clear();
            foreach (var armorName in Enum.GetNames(typeof(ArmorNameEnumMage)))
            {
                ArmorSet.Add(new Armor(
                    armorName: armorName,
                    armorValue: randomGenerator.Next(0, 5),
                    magicResist: randomGenerator.Next(10, 20),
                    speedAttribute: randomGenerator.Next(5, 15),
                    extraDamage: randomGenerator.Next(10, 20),
                    extraMana: randomGenerator.Next(50, 100),
                    hitChance: randomGenerator.Next(5,15)));
            }
            //Update character attributes
            UpdateValuesFromArmor();
        }
        public override void UpdateValuesFromArmor()
        {
            Armor = GetAllArmorValues();
            MagicResist += GetAllMagicResist();
            MovingSpeed += GetAllSpeedAttributes();
            MaxAttackDamage += GetAllExtraDamage();
            MinAttackDamage += GetAllExtraDamage();
            MaxMana += GetAllExtraMana();
            AttackChance += GetAllHitChance();
        }
        

        public int IncreaseMana(int manaGain)
        {
            Mana += manaGain;
            if (Mana >= MaxMana) Mana = MaxMana;
            return Mana;
        }

        public int ReduceMana(int manaLoss)
        {
            Mana -= manaLoss;
            if (Mana <= 0) Mana = 0;
            return Mana;
        }

        public void SetDefaultMana(int maxMana)
        {
            MaxMana = maxMana;
        }

    }
}

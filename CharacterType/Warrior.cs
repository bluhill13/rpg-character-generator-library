﻿using RPGCharacterStructure.CharacterType.WarriorType;
using RPGCharacterStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType
{
    public abstract class Warrior : Character, IRage
    {
        public int Rage{ get; set; }
        public int MaxRage { get; set; }
        public int RageUsePerAttack { get; set; }
        /// <summary>
        /// Top level character type
        /// </summary>
        /// <param name="characterName"></param>
        /// <param name="hp"></param>
        /// <param name="movingSpeed"></param>
        /// <param name="maxAttackDamage"></param>
        /// <param name="minAttackDamage"></param>
        /// <param name="attackChance"></param>
        /// <param name="armor"></param>
        /// <param name="magicResist"></param>
        public Warrior(string characterName,
            int hp,
            int movingSpeed,
            int maxAttackDamage,
            int minAttackDamage,
            int attackChance,
            int armor,
            int magicResist)
        {
            Name = characterName;
            MaxHp = hp;
            Hp = hp;
            MovingSpeed = movingSpeed;
            MaxMovingSpeed = movingSpeed;
            MaxAttackDamage = maxAttackDamage;
            MinAttackDamage = minAttackDamage;
            AttackChance = attackChance;
            Armor = armor;
            MagicResist = magicResist;
            CanAttack = true;
            CanMove = true;
            enumType = CharacterTypeEnumToHelpCasting.Warrior;
        }

        public override void ConfigureArmor()
        {
            //Clear list to not have duplicates
            ArmorSet.Clear();
            foreach (var armorName in Enum.GetNames(typeof(ArmorNameEnumWarrior)))
            {
                ArmorSet.Add(new Armor(
                    armorName: armorName,
                    armorValue: randomGenerator.Next(5, 15),
                    magicResist: randomGenerator.Next(0, 10),
                    speedAttribute: randomGenerator.Next(0, 2),
                    extraDamage: randomGenerator.Next(0, 5),
                    extraRage: randomGenerator.Next(0, 10),
                    hitChance: randomGenerator.Next(5, 10)));
            }
            //Update character attributes
            UpdateValuesFromArmor();
        }
        public override void UpdateValuesFromArmor()
        {
            Armor = GetAllArmorValues();
            MagicResist += GetAllMagicResist();
            MovingSpeed += GetAllSpeedAttributes();
            MaxAttackDamage += GetAllExtraDamage();
            MinAttackDamage += GetAllExtraDamage();
            MaxRage += GetAllExtraRage();
            AttackChance += GetAllHitChance();
        }

        public int IncreaseRage(int rageIncrease)
        {
            Rage += rageIncrease;
            if (Rage >= MaxRage) Rage = MaxRage;
            return Rage;
        }

        public int ReduceRage(int rageReduce)
        {
            Rage -= rageReduce;
            if (Rage <= 0) Rage = 0;
            return Rage;
        }

        public void SetDefaultRage(int maxRage)
        {
            MaxRage = maxRage;
        }
    }
}

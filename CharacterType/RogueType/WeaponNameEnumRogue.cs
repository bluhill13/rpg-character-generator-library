﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.RogueType
{
    /// <summary>
    /// Enum for rogue weapon choices
    /// </summary>
    public enum WeaponNameEnumRogue
    {
        OneHandSword,
        OneHandAxe,
        Dagger,
        Knife,
        PoleArm,
        AssasinCreedHiddenBlade
    }
}

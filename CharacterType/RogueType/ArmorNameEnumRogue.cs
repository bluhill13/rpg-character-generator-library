﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.RogueType
{
    /// <summary>
    /// Enum for rogue armor
    /// </summary>
    public enum ArmorNameEnumRogue
    {
        Balaclava,
        InvisibilityCloak,
        LeatherChest,
        Trouser,
        QuietShoes
    }
}

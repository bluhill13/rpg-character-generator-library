﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.CharacterType.RogueType
{
    public class AssasinationRogue : Rogue
    {
        /// <summary>
        /// Constructor where user of dll can choose himself
        /// what default values should be or just provide a name
        /// and use default values already provided
        /// </summary>
        /// <param name="characterName">required</param>
        /// <param name="hp">not required</param>
        /// <param name="movingSpeed">not required</param>
        /// <param name="maxAttackDamage">not required</param>
        /// <param name="minAttackDamage">not required</param>
        /// <param name="attackChance">not required</param>
        /// <param name="armor">not required</param>
        /// <param name="magicResist">not required</param>
        /// <param name="maxEnergy">not required</param>
        /// <param name="energyUsePerAttack">not required</param>
        public AssasinationRogue(
            string characterName,
            int hp = 2000,
            int movingSpeed = 150,
            int maxAttackDamage = 20,
            int minAttackDamage = 10,
            int attackChance = 20,
            int armor = 300,
            int magicResist = 400,
            int maxEnergy = 1000,
            int energyUsePerAttack = 50) : base(
                                         characterName,
                                         hp,
                                         movingSpeed,
                                         maxAttackDamage,
                                         minAttackDamage,
                                         attackChance,
                                         armor,
                                         magicResist)
        {
            Type = this.GetType().Name;
            MaxEnergy = maxEnergy;
            Energy = maxEnergy;
            EnergyUsePerAttack = energyUsePerAttack;

        }
        public override void ConfigureWeapon()
        {

            //Clear list to not have duplicates
            WeaponSet.Clear();
            //Rogue can have two weapons
            for (int i = 0; i < MAX_WEAPONS; i++)
            {
                double temp = randomGenerator.NextDouble();
                double speed = temp * (double)randomGenerator.Next(1, 2);
                WeaponSet.Add(new Weapon(
                    weaponName: Enum.GetName(typeof(WeaponNameEnumRogue), randomGenerator.Next(0, 5)),
                    weaponDamage: randomGenerator.Next(100, 300),
                    magicResist: randomGenerator.Next(0, 10),
                    weaponSpeed: speed,
                    extraEnergy: randomGenerator.Next(0, 10),
                    hitChance: randomGenerator.Next(10, 20)));
            }

            //Update character attributes
            UpdateValuesFromWeapon();
        }
        public void UpdateValuesFromWeapon()
        {
            WeaponDamagePerSecond = GetTotalWeaponDamagePerSecond();
            MaxAttackDamage += GetAllWeaponDamage();
            MinAttackDamage += GetAllWeaponDamage();
            MagicResist += GetAllMagicResistWeapon();
            MaxEnergy += GetAllExtraEnergyWeapon();
            AttackChance += GetAllHitChanceWeapon();
        }

        public override bool IsWeaponForMage()
        {
            foreach (Weapon piece in WeaponSet)
            {
                if (piece.MageWeapon) return true;
            }
            return false;
        }
    }
}

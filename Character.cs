﻿using RPGCharacterStructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure
{
    public abstract class Character : IHp, IMove, IAttack, IArmor, IWeapon
    {
        public Random randomGenerator = new Random();

        public List<Armor> ArmorSet = new List<Armor>();
        public List<Weapon> WeaponSet = new List<Weapon>();
        public const int MAX_ARMOR_PIECES = 5;
        public const int MAX_WEAPONS = 2;
        public string Name { get; set; }
        public string Type { get; set; }
        public int Hp { get; set; }
        public int MaxHp { get; set; }
        public bool CanMove { get; set; } = true;
        public int MovingSpeed { get; set; }
        public int MaxMovingSpeed { get; set; }
        public bool CanAttack { get; set; } = true;
        public double WeaponDamagePerSecond { get; set; }
        public int MaxAttackDamage { get; set; }
        public int MinAttackDamage { get; set; }
        public int AttackChance { get; set; }
        public int Armor { get; set; }
        public int MagicResist { get; set; }

        public CharacterTypeEnumToHelpCasting enumType;

        #region Hp
        public int HpIncrease(int healAmount)
        {
            Hp += healAmount;
            if (Hp >= MaxHp) Hp = MaxHp;
            return Hp;
        }

        public int HpReduce(int damageAmount)
        {
            Hp -= damageAmount;
            if (Hp <= 0) Hp = 0;
            return Hp;
        }
        public void SetDefaultHp(int maxHp)
        {
            MaxHp = maxHp;
        }


        #endregion

        #region Moving Speed
        public bool CanCharacterMove(bool change = false)
        {
            if (change && CanMove) { CanMove = false; }
            else if (change && !CanMove) { CanMove = true; }
            return CanMove;
        }
        public int IncreaseMovingSpeed(int speedIncrease)
        {
            MovingSpeed += speedIncrease;
            return MovingSpeed;
        }

        public int ReduceMovingSpeed(int speedDecrease)
        {
            MovingSpeed -= speedDecrease;
            return MovingSpeed;
        }

        public void ResetMovingSpeed()
        {
            MovingSpeed = MaxMovingSpeed;
        }

        

        public void SetDefaultMaxMovingSpeed(int speed)
        {
            MaxMovingSpeed = speed;
        }


        #endregion

        #region Attack
        public bool NotAbleToAttack(bool change = false)
        {
            if (CanAttack && change) CanAttack = false;
            else if (!CanAttack && change) CanAttack = true;
            return CanAttack;
        }

        public void Attack(Character character)
        {
            //Will attack another character
            //TODO: implement parameter for a character or other means to perform an attack
            int damageToBeDoneInAttack = SuccessfullAttack();
            character.HpReduce(damageToBeDoneInAttack);
        }
        
        public int GenerateAttackDamage()
        {
            return randomGenerator.Next(MinAttackDamage, MaxAttackDamage);
        }

        public int SuccessfullAttack()
        {
            int result = randomGenerator.Next(0, 100);
            if (result <= AttackChance) return GenerateAttackDamage();
            else return 0;
        }

        public void AttackSetup(int max, int min, int chance)
        {
            MaxAttackDamage = max;
            MinAttackDamage = min;
            AttackChance = chance;
        }

        #endregion

        #region Armor
        /// <summary>
        /// Is class specific armor configuration
        /// </summary>
        public abstract void ConfigureArmor();
        /// <summary>
        /// Updates character attributes 
        /// according to character type
        /// </summary>
        public abstract void UpdateValuesFromArmor();



        public int GetAllArmorValues()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.ArmorValue;
            }
            return totValue;
        }

        public int GetAllMagicResist()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.MagicResist;
            }
            return totValue;
        }

        public int GetAllSpeedAttributes()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.SpeedAttribute;
            }
            return totValue;
        }

        public int GetAllExtraDamage()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.ExtraDamage;
            }
            return totValue;
        }

        public int GetAllExtraMana()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.ExtraMana;
            }
            return totValue;
        }

        public int GetAllExtraEnergy()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.ExtraEnergy;
            }
            return totValue;
        }

        public int GetAllExtraRage()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.ExtraRage;
            }
            return totValue;
        }

        public int GetAllHitChance()
        {
            int totValue = 0;
            foreach (Armor piece in ArmorSet)
            {
                totValue += piece.HitChance;
            }
            return totValue;
        }

        #endregion

        #region Weapons

        public abstract void ConfigureWeapon();
        public abstract bool IsWeaponForMage();
        public int GetAllWeaponDamage()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.WeaponDamage;
            }
            return totValue;
        }

        public double GetTotalWeaponDamagePerSecond()
        {
            double totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                //calculating damage per second for each weapon or only one
                totValue += piece.WeaponDamage * (1.0 / piece.WeaponSpeed);
            }
            return totValue;
        }

        public int GetAllMagicResistWeapon()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.MagicResist;
            }
            return totValue;
        }

        public int GetAllExtraManaWeapon()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.ExtraMana;
            }
            return totValue;
        }

        public int GetAllExtraEnergyWeapon()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.ExtraEnergy;
            }
            return totValue;
        }

        public int GetAllExtraRageWeapon()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.ExtraRage;
            }
            return totValue;
        }

        public int GetAllHitChanceWeapon()
        {
            int totValue = 0;
            foreach (Weapon piece in WeaponSet)
            {
                totValue += piece.HitChance;
            }
            return totValue;
        }

        #endregion

    }
}

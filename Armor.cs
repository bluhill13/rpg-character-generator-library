﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure
{
    public class Armor
    {
        /// <summary>
        /// Constructor for Armor piece, 
        /// only required parameter is armorName
        /// </summary>
        /// <param name="armorName">required</param>
        /// <param name="armorValue">not required, default = 10</param>
        /// <param name="magicResist">not required, default = 1</param>
        /// <param name="speedAttribute">not required, default = 1</param>
        /// <param name="extraDamage">not required, default = 1</param>
        /// <param name="extraMana">not required, default = 1</param>
        /// <param name="extraEnergy">not required, default = 1</param>
        /// <param name="extraRage">not required, default = 1</param>
        /// <param name="hitChance">not required, default = 0</param>
        public Armor(string armorName,
            int armorValue = 10,
            int magicResist = 1,
            int speedAttribute = 0,
            int extraDamage = 0,
            int extraMana = 0,
            int extraEnergy = 0,
            int extraRage = 0,
            int hitChance = 0)
        {
            ArmorName = armorName;
            ArmorValue = armorValue;
            MagicResist = magicResist;
            SpeedAttribute = speedAttribute;
            ExtraDamage = extraDamage;
            ExtraMana = extraMana;
            ExtraEnergy = extraEnergy;
            ExtraRage = extraRage;
            HitChance = hitChance;
        }
        /// <summary>
        /// Name of armor
        /// </summary>
        public string ArmorName { get; set; }
        /// <summary>
        /// Reduce of damage
        /// </summary>
        public int ArmorValue{ get; set; }
        /// <summary>
        /// Reduce of magic damage
        /// </summary>
        public int MagicResist { get; set; }
        /// <summary>
        /// Reduce or increase of speedstat
        /// </summary>
        public int SpeedAttribute { get; set; }
        /// <summary>
        /// Extra damage from using gear
        /// </summary>
        public int ExtraDamage { get; set; }
        /// <summary>
        /// Extra mana from using gear
        /// </summary>
        public int ExtraMana { get; set; }
        /// <summary>
        /// Extra energy from using gear
        /// </summary>
        public int ExtraEnergy{ get; set; }
        /// <summary>
        /// Extra rage from using gear
        /// </summary>
        public int ExtraRage { get; set; }
        /// <summary>
        /// Extra added hitchance for an armor piece
        /// </summary>
        public int HitChance { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure
{
    public class Weapon
    {
        /// <summary>
        /// Constructor for weapon. 
        /// Only required parameter is weaponName
        /// </summary>
        /// <param name="weaponName">required</param>
        /// <param name="weaponDamage">not required, default = 10</param>
        /// <param name="magicResist">not required, default = 0</param>
        /// <param name="weaponSpeed">not required, default = 1.0</param>
        /// <param name="extraMana">not required, default = 0</param>
        /// <param name="extraEnergy">not required, default = 0</param>
        /// <param name="extraRage">not required, default = 1</param>
        /// <param name="hitChance">not required, default = 20</param>
        public Weapon(
            string weaponName,
            int weaponDamage = 10,
            int magicResist = 0,
            double weaponSpeed = 1.0,
            int extraMana = 0,
            int extraEnergy = 0,
            int extraRage = 0,
            int hitChance = 20,
            bool mageWeapon = false)
        {
            WeaponName = weaponName;
            WeaponDamage = weaponDamage;
            MagicResist = magicResist;
            WeaponSpeed = weaponSpeed;
            ExtraMana = extraMana;
            ExtraEnergy = extraEnergy;
            ExtraRage = extraRage;
            HitChance = hitChance;
            MageWeapon = mageWeapon;
        }
        public bool MageWeapon { get; set; }
        /// <summary>
        /// Name of armor
        /// </summary>
        public string WeaponName { get; set; }
        /// <summary>
        /// Reduce of damage
        /// </summary>
        public int WeaponDamage { get; set; }
        /// <summary>
        /// Reduce of magic damage
        /// </summary>
        public int MagicResist { get; set; }
        /// <summary>
        /// Reduce or increase of speedstat
        /// </summary>
        public double WeaponSpeed { get; set; }
        /// <summary>
        /// Extra mana from using gear
        /// </summary>
        public int ExtraMana { get; set; }
        /// <summary>
        /// Extra energy from using gear
        /// </summary>
        public int ExtraEnergy { get; set; }
        /// <summary>
        /// Extra rage from using gear
        /// </summary>
        public int ExtraRage { get; set; }
        /// <summary>
        /// Chance to hit with weapon
        /// </summary>
        public int HitChance { get; set; }
    }
}

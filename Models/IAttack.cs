﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IAttack
    {
        /// <summary>
        /// Will return of the character is able or not to attack
        /// in case of stun etc (not implemented)
        /// </summary>
        /// <param name="change"></param>
        /// <returns></returns>
        bool NotAbleToAttack(bool change = false);
        /// <summary>
        /// Attacks a character
        /// </summary>
        /// <param name="character"></param>
        void Attack(Character character);
        /// <summary>
        /// Generates attack damage based on a random number
        /// inbetween min and max damage
        /// </summary>
        /// <returns></returns>
        int GenerateAttackDamage();
        /// <summary>
        /// Depending on hitchance and a random number 
        /// it will allow character to attack with value of GenerateAttackDamage()
        /// or return 0
        /// </summary>
        /// <returns></returns>
        int SuccessfullAttack();
        /// <summary>
        /// Can change or set max and minimum damage and attackchance defaults
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="chance"></param>
        void AttackSetup(int max, int min, int chance);
    }
}

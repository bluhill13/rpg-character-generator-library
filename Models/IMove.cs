﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IMove
    {
        /// <summary>
        /// when moving speed is 0 it will be true or when otherwise called opon.
        /// </summary>
        /// <returns></returns>
        bool CanCharacterMove(bool change = false);
        /// <summary>
        /// Method to increase character moving speed,
        /// the result will go over maxmovingspeed until reset
        /// </summary>
        /// <param name="speedIncrease"></param>
        /// <returns></returns>
        int IncreaseMovingSpeed(int speedIncrease);
        /// <summary>
        /// Method to decrease character moving speed,
        /// the result can be 0 and therefore take use of CanMove function
        /// </summary>
        /// <param name="speedDecrease"></param>
        /// <returns></returns>
        int ReduceMovingSpeed(int speedDecrease);
        /// <summary>
        /// Resets the moving speed back to MaxMovingspeed/default speed
        /// </summary>
        void ResetMovingSpeed();
        /// <summary>
        /// Will set the default moving speed for a character, 
        /// this is the value it will be reverted back to 
        /// </summary>
        /// <param name="speed"></param>
        void SetDefaultMaxMovingSpeed(int speed);
    }
}

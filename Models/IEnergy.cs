﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IEnergy
    {
        int ReduceEnergy(int eneryUsed);
        int IncreaseEnergy(int energyGained);
        void SetDefaultEnergy(int maxEnergy);
    }
}

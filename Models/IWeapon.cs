﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    interface IWeapon
    {
        /// <summary>
        /// Configure all weapons for character
        /// </summary>
        void ConfigureWeapon();
        /// <summary>
        /// Return true if weapon if for a mage
        /// </summary>
        /// <returns></returns>
        bool IsWeaponForMage();
        /// <summary>
        /// Return the total Weapon Damage 
        /// of all weapons for character
        /// </summary>
        /// <returns></returns>
        int GetAllWeaponDamage();
        /// <summary>
        /// Return the total of magic resist 
        /// of all weapons for character
        /// </summary>
        /// <returns></returns>
        int GetAllMagicResistWeapon();
        /// <summary>
        /// Return the total value of damage per second
        /// from all weapons
        /// </summary>
        /// <returns></returns>
        double GetTotalWeaponDamagePerSecond();
        /// <summary>
        /// Returns the total value of extra mana attribute
        /// from all weapons
        /// </summary>
        /// <returns></returns>
        int GetAllExtraManaWeapon();
        /// <summary>
        /// Returns the total value of extra energy attribute
        /// from all weapons
        /// </summary>
        /// <returns></returns>
        int GetAllExtraEnergyWeapon();
        /// <summary>
        /// Returns the total value of extra rage attribute
        /// for all weapons
        /// </summary>
        /// <returns></returns>
        int GetAllExtraRageWeapon();
        /// <summary>
        /// Returns the total value of hitchance attribute
        /// from all weapons
        /// </summary>
        /// <returns></returns>
        int GetAllHitChanceWeapon();
    }
}

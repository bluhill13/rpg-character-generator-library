﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IArmor
    {
        /// <summary>
        /// Configure all armor for character
        /// </summary>
        void ConfigureArmor();
        /// <summary>
        /// Return the total armor value 
        /// of all armor for character
        /// </summary>
        /// <returns></returns>
        int GetAllArmorValues();
        /// <summary>
        /// Return the total of magic resist 
        /// of all armor for character
        /// </summary>
        /// <returns></returns>
        int GetAllMagicResist();
        /// <summary>
        /// Return the total value of speed attributes
        /// from all armor
        /// </summary>
        /// <returns></returns>
        int GetAllSpeedAttributes();
        /// <summary>
        /// Returns the total value of extra damage attribute
        /// from all armor
        /// </summary>
        /// <returns></returns>
        int GetAllExtraDamage();
        /// <summary>
        /// Returns the total value of extra mana attribute
        /// from all armor
        /// </summary>
        /// <returns></returns>
        int GetAllExtraMana();
        /// <summary>
        /// Returns the total value of extra energy attribute
        /// from all armor
        /// </summary>
        /// <returns></returns>
        int GetAllExtraEnergy();
        /// <summary>
        /// Returns the total value of extra rage attribute
        /// for all armor
        /// </summary>
        /// <returns></returns>
        int GetAllExtraRage();
        /// <summary>
        /// Returns the total value of hitchance attribute
        /// from all armor
        /// </summary>
        /// <returns></returns>
        int GetAllHitChance();
    }
}

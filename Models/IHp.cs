﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IHp
    {
        /// <summary>
        /// Function for reducing hp of a given character,
        /// should not be able to increase above max hp
        /// </summary>
        /// <param name="damageAmount"></param>
        /// <returns></returns>
        int HpReduce(int damageAmount);
        /// <summary>
        /// Function for increasing hp of a given character, 
        /// should not be able to increase above max hp
        /// </summary>
        /// <param name="healAmount"></param>
        /// <returns></returns>
        int HpIncrease(int healAmount);
        /// <summary>
        /// Function to increase the max hp a character have
        /// </summary>
        /// <param name="maxHp"></param>
        void SetDefaultHp(int maxHp);
    }
}

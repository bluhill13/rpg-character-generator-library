﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IMana
    {
        /// <summary>
        /// Reduces mana ba manaLoss amount and returns the total mana
        /// </summary>
        /// <param name="manaLoss"></param>
        /// <returns></returns>
        int ReduceMana(int manaLoss);
        /// <summary>
        /// Increases mana by manaGain amount and returns the total mana
        /// </summary>
        /// <param name="manaLoss"></param>
        /// <returns></returns>
        int IncreaseMana(int manaGain);
        /// <summary>
        /// Call this to set default mana to a new value
        /// </summary>
        /// <param name="maxMana"></param>
        void SetDefaultMana(int maxMana);
    }
}

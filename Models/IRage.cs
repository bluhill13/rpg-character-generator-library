﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterStructure.Models
{
    public interface IRage
    {
        /// <summary>
        /// Function for reducing rage of a given character,
        /// should not be able to increase above max rage
        /// </summary>
        /// <param name="damageAmount"></param>
        /// <returns></returns>
        int ReduceRage(int rageReduce);
        /// <summary>
        /// Function for increasing rage of a given character, 
        /// should not be able to increase above max rage
        /// </summary>
        /// <param name="healAmount"></param>
        /// <returns></returns>
        int IncreaseRage(int rageIncrease);
        /// <summary>
        /// Function to increase the max rage a character have
        /// </summary>
        /// <param name="maxHp">the value for maximum rage to be set</param>
        void SetDefaultRage(int maxRage);
    }
}
